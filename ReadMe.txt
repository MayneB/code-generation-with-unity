Hey!

Thanks for downloading the Code Generation with Unity demo project. This repo is pretty bare bones but is designed to allow
the client to work in a Standalone CS project and still have their scripts in Unity. I have extended MSBuild to export all
scripts (ignoring AssemblyInfo) using a wildcard so they can be named however you like. To use the MSBuild scripts you will 
need to use one of the following

This codebase is to be used with the tutorial posted on Gamasutra

http://gamasutra.com/blogs/ByronMayne/20160121/258356/Code_Generation_in_Unity.php 

Windows:
 - Visual Studio
 - Xamarin

OSX
 - Xamarain (In theory but I don't own a Mac to test this on.) 

Folder layout
=============
proj.cs
   - Contains all our source code.
   - Exports our scripts on successful build to proj.unity

proj.unity
   - Contains our Unity project and our exported scripts.


Notes 
=============
- In theory this should work fine on Mac with MonoDevelop but I don't have a mac so I can't test this. It for
  sure works in MonoDevelop on Windows.

- I have also uploaded the full complete project but put it in it's own branch. Checkout the Branch "Completed" to view
  the source.


- This project does have some custom build steps and I have also documented them with comments. You can find the two scripts
  under Properties/ResolveUnityRefrences.xml and Properties/UnityScriptExporter.xml. 


If you have any questions or find any problems please feel free to reach out to me 
T: @ByMayne
E: ByronMayne@gmail.com